<?php
$heights = [180, 175, 183, 192, 177];

$names = ['Ivanov', 'Petrov', 'Sidorov', 'Morozov', 'Orlov'];

$max_height = max($heights);

$max_height_key = array_search($max_height, $heights);

foreach($names as $key => $value) {
    if($key === $max_height_key) {
        print_r($value);
    }
}
