<?php

$array = [];
for ($i = 0; $i < 10; $i++) {
    $array[] = rand(1, 100);
}

$max_number = max($array);

print "Массив элементов:\n\n";

foreach ($array as $key => $value) {
    print $value . " ";
}

print "\n\nМаксимальное число: {$max_number}";

foreach($array as $key => $value) {
    if($value == $max_number) {
        $new_array = array_slice($array, 0, $key, true);
        $total_number_of_elements_before_last_max = count($new_array);
    }
}

print "\n\nКоличество элементов перед последним максимальным числом: {$total_number_of_elements_before_last_max}\n";
