<?php
$win = [];
for ($i = 0; $i < 20; $i++) {
    $win[] = rand(1, 7);
}

$lost = [];
for ($i = 0; $i < 20; $i++) {
    $lost[] = rand(1, 7);
}

$goals = array_map(function ($key, $val) {
    return array($key => $val);
}, $win, $lost);

$count = 1;

foreach ($goals as $key => $value) {
    foreach ($value as $a => $b) {
        if ($a > $b) {
            print "Результат матча №{$count}: Выигрыш ({$a}:{$b})!\n";
        } elseif ($a < $value) {
            print "Результат матча №{$count}: Проигрыш ({$a}:{$b})!\n";
        } else {
            print "Результат матча №{$count}: Ничья ({$a}:{$b})!\n";
        }
    }
    $count++;
}
