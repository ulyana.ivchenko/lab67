<?php
$array1 = [2, 4, 6, 8, 10, 12, 14];
$array2 = [2, 4, 6, 8, 10, 11, 14];

function isArithmeticProgression ($array) {
    $d = $array[1] - $array[0];
    for($i = 2; $i < count($array); $i++) {
        if(($array[$i] - $array[$i - 1]) != $d)
            return "nil";
    }
    return "Разность арифметической прогресии = " . $d . "\n";
}

$result1 = isArithmeticProgression($array1);
$result2 = isArithmeticProgression($array2);

print $result1;
print $result2;
